import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { parse } from 'yaml';

export class OpenapiArrayItemDef {
    constructor (public type: OpenapiSchemaDef) {}
    minItems: number = 0;
    maxItems?: number;
}

export class OpenapiSchemaDef {
    name: string = '<empty>';
    type: 'integer' | 'number' | 'string' | 'boolean' | 'array' | 'object' = 'string';
    format?: 'int32' | 'int64' | 'float' | 'double' | 'byte' | 'binary' | 'date' | 'date-time' | 'password';
    mandatory: boolean = false;
    enum: string[] = [];
    default?: string;
    description?: string;
    minLength?: number;
    maxLength?: number;
    objectProperties: OpenapiSchemaDef[] = [];
    arrayItemType?: OpenapiArrayItemDef;
}

export class OpenapiFormField {
    constructor (public def: OpenapiSchemaDef) {
        if (def.mandatory) {
            this.formControl.addValidators(Validators.required);
        }
    }
    formControl: FormControl = new FormControl('');
    isNull: boolean = true;
    objectProperties: OpenapiFormField[] = [];
    arrayItems: OpenapiFormField[] = [];
    parentField?: OpenapiFormField;
}

enum ResultMessageType {
    OPENAPI_SCHEMA_GUI_FINISHED = 'OPENAPI_SCHEMA_GUI_FINISHED',
}

enum ResultMessageSubType {
    SUBMIT = 'SUBMIT',
    CANCEL = 'CANCEL',
}

class ResultMessage {
    type: ResultMessageType = ResultMessageType.OPENAPI_SCHEMA_GUI_FINISHED;
    subType: ResultMessageSubType = ResultMessageSubType.CANCEL;
    instanceId: string = '';
    json: any = null;
}

@Component({
    selector: 'openapi-schema-gui',
    standalone: true,
    imports: [
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    templateUrl: './openapi-schema-gui.component.html',
    styleUrl: './openapi-schema-gui.component.scss',
    encapsulation: ViewEncapsulation.ShadowDom,
})
export class OpenapiSchemaGuiComponent {

    instanceId: string = 'instanceId_not_set';
    schemaUrl: string = '/assets/form_schemas.yaml';
    schemaName: string = 'ImpliedValueFormula';
    isArray: boolean = true;
    jsonString: string = '';
    exportNullAsUndefined: boolean = true;

    schemaLoadErrorMessage: string = '';

    schemaDef: OpenapiSchemaDef = new OpenapiSchemaDef();
    schemaRootField: OpenapiFormField = new OpenapiFormField(new OpenapiSchemaDef());

    static LOCAL_SCHEMA_PREFIX = '#/components/schemas/';
    static SCHEMA_MAP: Map<string, OpenapiSchemaDef> = new Map();

    constructor (private http: HttpClient, private elementRef: ElementRef) {}

    ngOnInit() {

        console.info('aaa');

        this.readParams();

        this.http.get(this.schemaUrl, { responseType: 'text' }).subscribe({
            next: (res: string) => {
                const rootObj = parse(res);
                
                const schemaObj = OpenapiSchemaGuiComponent.findSchemaObj(rootObj, this.schemaName);
                
                const schemaDef = OpenapiSchemaGuiComponent.mapSchemaObjToDef(rootObj, schemaObj, this.schemaName, true);
                if (this.isArray) {
                    const rootDef = new OpenapiSchemaDef();
                    rootDef.type = 'array';
                    rootDef.name = 'ArrayOf' + this.schemaName;
                    rootDef.mandatory = true;
                    rootDef.arrayItemType = new OpenapiArrayItemDef(schemaDef);
                    this.schemaDef = rootDef;
                } else {
                    this.schemaDef = schemaDef;
                    this.schemaDef.mandatory = true;
                }

                const jsonObj = this.jsonString.length > 0 ? JSON.parse(this.jsonString) : this.isArray ? [] : {};

                this.schemaRootField = OpenapiSchemaGuiComponent.fillSchemaDefWithJson(this.schemaDef, jsonObj);

                console.debug('schemaObj', schemaObj);
                console.info('this.schemaRootField', this.schemaRootField);
            },
            error: (err: any) => {
                console.error(err);
                this.schemaLoadErrorMessage = JSON.stringify(err);
            }
        });
    }

    readParams() {
        this.instanceId = this.getValueOrNullWrapperDataParam('data-instanceId') ?? this.instanceId;
        this.schemaUrl = this.getValueOrNullWrapperDataParam('data-schemaUrl') ?? this.schemaUrl;
        this.schemaName = this.getValueOrNullWrapperDataParam('data-schemaName') ?? this.schemaName;
        const isArrayRaw = this.getValueOrNullWrapperDataParam('data-isArray');
        this.isArray = isArrayRaw == null ? this.isArray : isArrayRaw === 'true';
        this.jsonString = this.getValueOrNullWrapperDataParam('data-jsonString') ?? this.jsonString;
        const exportNullAsUndefinedRaw = this.getValueOrNullWrapperDataParam('data-exportNullAsUndefined');
        if (exportNullAsUndefinedRaw != null) {
            this.exportNullAsUndefined = exportNullAsUndefinedRaw === 'true';
        }
        console.info('schemaUrl:', this.schemaUrl);
        console.info('schemaName:', this.schemaName);
        console.info('isArray:', this.isArray);
        console.info('jsonString:', this.jsonString);
    }

    getValueOrNullWrapperDataParam(paramName: string): string | null {
        const value = this.elementRef.nativeElement.getAttribute(paramName);
        return value?.length > 0 ? value : null;
    }

    static fillBasicDef(schemaObj: any, schemaDef: OpenapiSchemaDef, name: string, mandatory: boolean): void {
        schemaDef.type = schemaObj.type;
        schemaDef.name = name;
        schemaDef.mandatory = mandatory;
        schemaDef.format = schemaObj.format;
        schemaDef.default = schemaObj.default;
        schemaDef.description = schemaObj.description;
        schemaDef.enum = schemaObj.enum != null ? schemaObj.enum : [];
    }

    static findSchemaObj(obj: any, schemaName: string): any {
        return obj['components']['schemas'][schemaName];
    }

    static mapSchemaObjToDef(rootObj: any, schemaObjParam: any, fieldName: string, isDeclaredSchema: boolean = false): OpenapiSchemaDef {

        const rootDef = new OpenapiSchemaDef();

        const schemaRef = isDeclaredSchema ? this.LOCAL_SCHEMA_PREFIX + fieldName : schemaObjParam['$ref'];
        let schemaObj = schemaObjParam;
        if (schemaRef != null) {
            const refName: string = schemaRef;
            const referencedSchema = this.SCHEMA_MAP.get(refName);
            if (referencedSchema != null) {
                return referencedSchema;
            }
            let refSchemaObj = null;
            if (refName.startsWith(this.LOCAL_SCHEMA_PREFIX)) {
                const refSchemaName = refName.substring(this.LOCAL_SCHEMA_PREFIX.length);
                refSchemaObj = this.findSchemaObj(rootObj, refSchemaName);
            }
            if (refSchemaObj == null) {
                throw new Error('schema not found:' + refName);
            }
            schemaObj = refSchemaObj;

            this.SCHEMA_MAP.set(refName, rootDef);
        }

        if (schemaObj.allOf != null) {
            let initiallyFilled = false;
            for (const allOfItem of schemaObj.allOf) {
                const allOfItemSchemaDef = this.mapSchemaObjToDef(rootObj, allOfItem, fieldName);
                if (!initiallyFilled) {
                    initiallyFilled = true;
                    rootDef.type = allOfItemSchemaDef.type;
                    rootDef.name = fieldName;
                    rootDef.mandatory = false;
                    rootDef.format = allOfItemSchemaDef.format;
                    rootDef.default = allOfItemSchemaDef.default;
                    rootDef.description = allOfItemSchemaDef.description;
                    rootDef.enum = allOfItemSchemaDef.enum != null ? allOfItemSchemaDef.enum : [];
                    rootDef.objectProperties.push(...allOfItemSchemaDef.objectProperties);
                    rootDef.arrayItemType = allOfItemSchemaDef.arrayItemType;
                } else {
                    rootDef.objectProperties.push(...allOfItemSchemaDef.objectProperties);
                }
            }

            return rootDef;
        } else {
            this.fillBasicDef(schemaObj, rootDef, fieldName, false);
        }

        if (rootDef.type === 'object') {

            const requiredPropNames: string[] = schemaObj.required ?? [];

            for (const propName in schemaObj.properties) {
                const propObj: any = schemaObj.properties[propName];
                const propDef = this.mapSchemaObjToDef(rootObj, propObj, propName);
                propDef.mandatory = requiredPropNames.includes(propName);
                rootDef.objectProperties.push(propDef);
            }
        } else if (rootDef.type === 'array') {

            const itemsObj = schemaObj.items;
            const itemsDef = this.mapSchemaObjToDef(rootObj, itemsObj, fieldName);
            itemsDef.mandatory = true;
            const arrayDef = new OpenapiArrayItemDef(itemsDef);
            rootDef.arrayItemType = arrayDef;

        }

        return rootDef;
    }

    static fillSchemaDefWithJson(schemaDef: OpenapiSchemaDef, jsonObj: any): OpenapiFormField {

        const rootField = new OpenapiFormField(schemaDef);

        if (schemaDef.mandatory) {
            rootField.isNull = false;
        }

        if (schemaDef.type === 'object') {

            for (const propDef of schemaDef.objectProperties) {
                let jsonItem = jsonObj[propDef.name] ?? null;
                if (propDef.type === 'object' && jsonItem == null) {
                    jsonItem = {};
                } else if (propDef.type === 'array' && jsonItem == null) {
                    jsonItem = [];
                }
                const field = this.fillSchemaDefWithJson(propDef, jsonItem);
                rootField.objectProperties.push(field);
                field.parentField = rootField;
            }
            rootField.isNull = false;

        } else if (schemaDef.type === 'array') {

            console.info('array schemaDef', schemaDef.name);
            console.info('array jsonObj', jsonObj);

            for (const item of jsonObj ?? []) {
                const field = this.fillSchemaDefWithJson(schemaDef.arrayItemType!.type, item);
                rootField.arrayItems.push(field);
                field.parentField = rootField;
            }
            rootField.isNull = false;
            
        } else {

            const defaultValueStr = schemaDef.default ?? null;
            const resultValue = jsonObj == null ? defaultValueStr : jsonObj;

            rootField.formControl.setValue(resultValue);

            if (resultValue != null) {
                rootField.isNull = false;
            }
        }

        return rootField;
    }

    addObjectItem(emptyObjectField: OpenapiFormField) {
        console.debug('addObjectItem', emptyObjectField);
        const newField = OpenapiSchemaGuiComponent.fillSchemaDefWithJson(emptyObjectField.def, {});
        emptyObjectField.objectProperties.push(...newField.objectProperties);
        emptyObjectField.isNull = false;
    }

    removeObjectItem(filledObjectField: OpenapiFormField) {
        console.debug('removeObjectItem', filledObjectField);
        filledObjectField.objectProperties.length = 0;
        filledObjectField.isNull = true;
    }

    addArrayItem(arrayField: OpenapiFormField) {
        console.debug('addArrayItem', arrayField);
        const newField = OpenapiSchemaGuiComponent.fillSchemaDefWithJson(arrayField.def.arrayItemType!.type, {});

        console.debug('newField', newField);

        arrayField.arrayItems.push(newField);
    }

    removeArrayItem(arrayItemField: OpenapiFormField, parentArrayField: OpenapiFormField) {
        console.debug('removeArrayItem arrayItemField', arrayItemField);
        console.debug('removeArrayItem parentArrayField', parentArrayField);

        const index = parentArrayField.arrayItems.indexOf(arrayItemField);
        console.debug('removeArrayItem index', index);
        parentArrayField.arrayItems.splice(index, 1);
    }

    submit() {

        console.info('source: ', this.schemaRootField);

        const json = this.exportToJson();

        const message = new ResultMessage();
        message.subType = ResultMessageSubType.SUBMIT;
        message.json = json;

        console.info('result: ', json);
        console.info('result: ', JSON.stringify(json));

        this.closeApp(message);
    }

    cancel() {
        this.closeApp(new ResultMessage());
    }

    closeApp(message: ResultMessage) {
        message.instanceId = this.instanceId;
        console.info('postMessage: sending message in standard browser:', message);
        window.postMessage(message, window.location.origin);
    }

    exportToJson(field: OpenapiFormField | null = null): any {

        const fieldToExport = field ?? this.schemaRootField;

        if (fieldToExport.isNull) {
            return null;
        }

        const rawValue = fieldToExport.formControl.value;

        if (fieldToExport.def.type === 'object') {

            const json: any = {};
            for (const objField of fieldToExport.objectProperties) {
                const value = this.exportToJson(objField);
                if (value != null || !this.exportNullAsUndefined) {
                    json[objField.def.name] = value;
                }
            }
            return json;

        } else if (fieldToExport.def.type === 'array') {

            const json: any = [];
            for (const itemField of fieldToExport.arrayItems) {
                json.push(this.exportToJson(itemField));
            }
            return json;

        } else if (fieldToExport.def.type === 'number') {

            return rawValue == null ? null : Number.parseFloat(rawValue);

        } else if (fieldToExport.def.type === 'integer') {

            return rawValue == null ? null : Number.parseInt(rawValue);

        } else if (fieldToExport.def.type === 'boolean') {

            return rawValue == null ? null : rawValue === true;

        }

        return String(rawValue ?? '');
    }
}
