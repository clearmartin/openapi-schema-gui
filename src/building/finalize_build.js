const concat = require('concat');
(async () => {
    const appName = 'openapi-schema-gui';
    const distPath = './dist';

    const files = [
        `./src/building/output_script_prelude.js`,
        `${distPath}/${appName}/browser/polyfills.js`,
        `${distPath}/${appName}/browser/main.js`,
        `./src/building/output_script_epilog.js`,
    ]

    const outputFilePath = `${distPath}/${appName}.js`;
    await concat(files, outputFilePath);

})();
