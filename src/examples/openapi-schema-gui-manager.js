'use strict';

(() => {

    const OpenapiSchemaGuiManager = {};
    OpenapiSchemaGuiManager.JS_URL = '/dist/openapi-schema-gui.js';
    OpenapiSchemaGuiManager.openapiSchemaGuiInstances = [];

    const messageListener = (event) => {

        console.info('message arrived', event);

        // optional security
        if (event.origin !== window.location.origin) {
            console.warn('somebody else hijacked messaging');
            return;
        }

        if (event.data?.type === 'OPENAPI_SCHEMA_GUI_FINISHED') {
            // do something about it
            //...

            const instanceId = event.data.instanceId;

            if (event.data.subType === 'SUBMIT') {
                console.info('jsonValue:', event.data.json);
            } else {
                console.info('CANCEL');
            }

            OpenapiSchemaGuiManager.openapiSchemaGuiInstances.filter(item => item.instanceId === instanceId).forEach(item => {
                document.body.removeChild(item.appElem);
                document.head.removeChild(item.scriptElem);
                item.itemHandler(event.data);
            });
        }
    }

    window.addEventListener('message', messageListener);

    OpenapiSchemaGuiManager.showForm = (schemaUrl, schemaName, isArray, jsonString, itemHandler) => {

        const appElem = document.createElement('openapi-schema-gui');

        const instanceId = String(new Date().getTime());

        appElem.setAttribute('data-instanceId', instanceId);
        appElem.setAttribute('data-schemaUrl', schemaUrl);
        appElem.setAttribute('data-schemaName', schemaName);
        appElem.setAttribute('data-isArray', String(isArray));
        appElem.setAttribute('data-jsonString', jsonString);
        appElem.innerHTML = 'Loading...';

        document.body.appendChild(appElem);

        const scriptElem = document.createElement('script');
        scriptElem.setAttribute('src', OpenapiSchemaGuiManager.JS_URL);
        document.head.appendChild(scriptElem);

        const defaultHandler = (item) => {};

        OpenapiSchemaGuiManager.openapiSchemaGuiInstances.push({
            instanceId: instanceId,
            appElem: appElem,
            scriptElem: scriptElem,
            itemHandler: itemHandler ?? defaultHandler,
        });

        console.info('init done');
    };

    window['OpenapiSchemaGuiManager'] = OpenapiSchemaGuiManager;
})();
