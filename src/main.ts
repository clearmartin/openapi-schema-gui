import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { OpenapiSchemaGuiComponent } from './app/openapi-schema-gui.component';

bootstrapApplication(OpenapiSchemaGuiComponent, appConfig)
  .catch((err) => console.error(err));
